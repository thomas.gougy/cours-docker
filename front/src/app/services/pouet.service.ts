import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {VehicleDto} from "../vehicle/vehicle.dto";

@Injectable({
  providedIn: 'root'
})
export class PouetService {

  constructor( private httpClient: HttpClient ) { }


  getAllVehicle(){
    return new Promise((resolve, reject) => {
      this.httpClient.get(
        environment.bdd + "/vehicle"
      ).subscribe(
        (vehicleList) => {
          resolve(vehicleList)
        },
        (error) => {
          reject(error)
        }
      )
    })
  }

  getOneVehicle(id:string){
    return new Promise((resolve, reject) => {
      this.httpClient.get(
        environment.bdd + "/vehicle/" + id
      ).subscribe(
        (vehicle) => {
          resolve(vehicle)
        },
        (error) => {
          reject(error)
        }
      )
    })
  }

  createOneVehicle(data: any){
    return new Promise<VehicleDto>((resolve, reject) => {
      this.httpClient.post<VehicleDto>(
        environment.bdd + "/vehicle",
        data
      ).subscribe(
        (vehicle) => {
          resolve(vehicle)
        },
        (error) => {
          reject(error)
        }
      )
    })
  }

  deleteOneVehicle(id: string){
    return new Promise((resolve, reject) => {
      this.httpClient.delete(
        environment.bdd + "/vehicle/"+ id
      ).subscribe(
        (vehicle) => {
          resolve(vehicle)
        },
        (error) => {
          reject(error)
        }
      )
    })
  }
}
