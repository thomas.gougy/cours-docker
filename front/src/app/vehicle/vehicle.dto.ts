export interface VehicleDto {
  id: string,
  brand: string,
  year: number,
  plate: string,
  model: string
}
