import { Component, OnInit } from '@angular/core';
import {PouetService} from '../services/pouet.service'
import {VehicleDto} from "./vehicle.dto";

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

  vehicleList: VehicleDto[] = []
  createdVehicle: {
    brand: string,
    year: number,
    plate: string,
    model: string
  } ={
    brand: "",
    year: 1900,
    plate: "",
    model: ""
  }

  constructor(private vehicleService: PouetService) { }

  ngOnInit(): void {
    this.vehicleService.getAllVehicle().then((list:any)=>{
      this.vehicleList = list
    })
  }

  createVehicle(){
    if(this.createdVehicle.brand != "" && this.createdVehicle.model != "" && this.createdVehicle.year != 1900 && this.createdVehicle.plate != "" ){
      this.vehicleService.createOneVehicle(this.createdVehicle).then((vehicle: VehicleDto)=>{
        this.vehicleList.push(vehicle)
        this.createdVehicle = {
          brand: "",
          year: 1900,
          plate: "",
          model: ""
        }
      })
    }
  }

}
