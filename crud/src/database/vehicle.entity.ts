import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Vehicle {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    brand: string;

    @Column()
    model: string;

    @Column()
    year: string

    @Column()
    plate: string

}