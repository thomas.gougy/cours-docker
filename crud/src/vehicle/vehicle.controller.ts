import {Body, Controller, Delete, Get, Param, Post} from '@nestjs/common';
import {VehicleService} from "./vehicle.service";

@Controller('vehicle')
export class VehicleController {

    constructor(private vehicleService: VehicleService) {
    }

    @Get()
    getVehicle(){
        return this.vehicleService.find()
    }

    @Get("/:id")
    getOneVehicle(@Param('id')id: string){
        return this.vehicleService.findOne(id)
    }

    @Post()
    postVehicle(@Body()data: any){
        return this.vehicleService.create(data)
    }
    @Delete("/:id")
    deleteVehicle(@Param('id')id: string){
        return this.vehicleService.remove(id)
    }

}
