import {Injectable, NotFoundException} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Vehicle} from "../database/vehicle.entity";
import {Repository} from "typeorm";

@Injectable()
export class VehicleService {

    constructor(
        @InjectRepository(Vehicle)
        private vehicleRepository: Repository<Vehicle>,
    ) { }

    find(){
        return this.vehicleRepository.find()
    }

    async findOne(id: string) {
        const vehicle = await this.vehicleRepository.findOneBy({ id })
        if(!vehicle){
            return new NotFoundException('véhicle not found')
        }
        return vehicle;
    }

    create(data: any){
        const vehicle = new Vehicle()
        vehicle.brand = data.brand
        vehicle.year = data.year
        vehicle.plate = data.plate
        vehicle.model = data.model

        return this.vehicleRepository.save(vehicle)
    }

    remove(id: string) {
        return this.vehicleRepository.delete(id);
    }

    async update(id:string , data: any){

    }
}
